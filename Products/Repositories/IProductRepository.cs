﻿using Products.Models;

using System.Collections.Generic;

namespace Products.Repositories
{
    /// <summary>
    /// Using an interface for loose coupling even in a demo
    /// </summary>
    public interface IProductRepository
    {
        // preferring a property to an accessor
        IList<ProductModel> Products { get; }
    }
}
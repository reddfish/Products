﻿using Products.Models;

using System.Collections.Generic;

namespace Products.Repositories
{
    /// <summary>
    ///     A mock implementation of a Repository from the Repository/Service pattern
    ///     This is the back, database-oriented layer of the business layer, but here it will just
    ///     provide data from a hard-coded list rather than access a database
    /// </summary>
    public class ProductRepository : IProductRepository
    {
        private IList<ProductModel> products = new List<ProductModel>()
        {
            new ProductModel()
            {
            ProductId="1",
            Name="Square",
            Price=10.1m,
            Stock=2,
            Image="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACAAAAAgCAYAAABzenr0AAABHklEQVR42sWXS1LCQBRF30NgHQzYl1VUUSXIR/kIQR2zE+aMwKDyVRRHjpAk2lFAEHYASPtSlFvI7VEy6lP3npdO88v1OsbMBhEFyd+101pXWQBseYn6vPn/cnh0tVJEHMHsr11+vvxRUgEEQCo4AMgzKAFy+amyxAIMjYViYgiA9hwYlheKGJSAlgQey9/YCh5Kc+wYDi7m4gAmAe0lMCjOsA70i1NsBb3CFFtBL/+FnYJu7lMcYAyAdxZ0PABkAu1zF+tA+0xhp+A++4F14E4AoBXcZt7BAGkH60Ar5WDPAjNlYyswTy1sBTdJC/slbCYmWAcaiTfsX3HjZIx1oB5/tUOBMORyut1vHK4d92NHgZDBPl/PZQR3e/1b/QNp1aUzggmj/wAAAABJRU5ErkJggg=="
            },
            new ProductModel()
            {
            ProductId="2",
            Name="Circle",
            Price=20.2m,
            Stock=3,
            Image="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACAAAAAgCAYAAABzenr0AAADP0lEQVR42mNkIBIY5vcxSVq4W3GJyTgBuQZArADEAlDpD0D8AIgvfH/7Yt+Lk7uOne3N/UeMuYyEFNj3bxcU0bXMBTJTgViGSPc+AeLZ726en7w/2/E9WQ7Qz+lmVPJLzmRkZGwBcgWJDSk0ALK85sHOpdPP9eT8J9oBDlP2CgmqGS4DMt3JtBgd7Pz04EbU3jSrdwQd4DL/jDSPtNIeIFODSpbDwI3vr5+67IzWfYrTAXYTdwkJahgfpYHlcEd8fnDDel+6NTwk4A7QSW9hVArM2M5IvWDHCoAJYeejXcs8L/Tl/UdxgO+2l1mMjExTaWk5kiOyN3uKToM7wKJtjaCoof1dBvJTO6ng/Ydb55UP57u9BzvAZ+vLOgZGxkY6WQ4Nhv/1W7zFmxg1kxuYlIIyHzIQX8hQCzx5vGupPKPd9MM2vLKqh+lsORj8ePfSltFz8/M6RnoHPxT8B0YDyAHrgOzAgXAAEKxn9Nj07ByQYThADjjP6L7x6T0gQ3GAHHCf0W3D43vAAmhAHPD//7/7jK7rHp5jZGIekCj4/+/veUaX1XfXMbKwDUgi/P/n93pGp2XX65g4uAcmG/76Xs9o1rbWhlfd5DCwKKaz7f8Zvj+5ZcuoFJrHpBCS+5CBhY2+RfGfX0+ebFsgD/a2w6JLdUxcfHSNhn/fP9cfiNVtAjtAp2CioIi5511GFla6VMfAxPf+w+Ujyhfakt7DI9527pksZl7BqUS01Cm1nuHf14/ZhxKNEA0SEJAPzGSUDwA2yTh43GmWIIEJ7/+PLzuf7lrqeXdpF2qTDAR0S2cICehYHWXk4KZJo/T/j683Pt++YH2hJQ6zUQoDhg3LpbkVtPZQ2xEgy78/u+tytioId7McBnRKpgvxa5otA0UHIxMTZRb/+wcO9s93L0Vdaksk3DGBATn/dEZpj/hMYCnZwsjGIchAqkNAFv/68f7fr281z/eunP5g1QTiu2bIQCOnV1BQ1zqXkZUtFVRYMTKzMuBMpKBE9vc3uJD5/+fX7I/XT0++1p9LXucUI0QCM5kEdK2tuKSVnYC1pwEDI5MC0CECUIs/AMP6AcO/vxe+vXiw78Pl48cerplEVPccAKa5MoQVj/lPAAAAAElFTkSuQmCC"
            },
            new ProductModel()
            {
                ProductId="3",
                Name="Triangle",
                Price=723.00m,
                Stock=12,
                Image="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACAAAAAgCAYAAABzenr0AAADEElEQVR42mNkoAB8WKIdzMbCyM4VcWUZuWYwUuKAHyt1jvz//5+DM+KqCd0dcLFPRVdNiv0iyIxXH/+YyqffPENXB3xbrj0FSGUzMPwHcedwRV5LpZsDFufJcAdb8j0FMvn/g+3//+XQta/Sni2PPtHFAR8WaaawsjDOhvqeAeQIYFrI4Y25MZUuDvi6TBMU38ZQ38MccQnoAH2aO+DqBGUTeVHW0zA+siPefflrLZ9x5xhNHfBlqcYcIJWM5nsYexFf7K14mjlgRaE0v7cxDyjxcaP7HsJm+H7y9jcZt+Yn72jigA+L1HKYmRgn4/A9lP+/SCD+Tj9NHPBpsdplIKWDw/dgPpB9QzDhjibVHXBzkpKNhADLYVTfYnfEp+//HOUz7x+gqgM+LlJdAqSikS1CdwQSf4VQ4r1IqjlgXamUiJMO92OgwRzovsXB/3nhwU8556anr6jigHfzVUqYmBi6cfgWlyMqRJLvd1LsgMYwYcY8L8GbQCNVCfkeTe7ukkOfVQsWvv2Pz3yCDrg1ScFFlI95N7G+/49k3def/9wVsh/tosgB7+YrrQZSIbh9jzMKQOR6sdRHQWQ7YFO5pKSNBudDoGGs+C3C9D2U//v6018KDo0vnpHlgLfzFKuBVAtui4hyVJ1E+uNmkh3QGiHElO7Kfw/IlCfSt7gc8Wj1ia9KufPf/yXJAXcmyXkLcDNtIcEinI768fufn2Lus80kOeD1HAWQBh8ishsxjtgqlfnUh2gH7KiSkDNWYr8H1MyMy/f4QgaL3N+7L/8o2zW+ekiUA17NlgclmhpCvsftCHC1jGImkN8ik/28lqADumOEWOLteR4B9UuSGMyE5J5vOvtDLmvehz94HXB3kkwwDwfTGvItQvc9Qu7Xn/8hygUv1+J1wIuZsruBlAslFuFRu1su96UbTgfsqRFX0ZFlA1U8TMT6lsSQ+ffozV9126a3d7A64PkMmW4gVULYt/h9j08tkN2tkP+6DKsDbk+Unc3E+N+FKINAifM/LjkG5FSLUmn9/cewV6fsVQpMDgA1Zig/Egu9gAAAAABJRU5ErkJggg=="
            }
        };

        /// <summary>
        /// Using a property rather than an accessor function GetProducts()
        /// </summary>
        IList<ProductModel> IProductRepository.Products => this.products;
    }
}
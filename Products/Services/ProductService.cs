﻿using Products.Models;
using Products.Repositories;

using System.Collections.Generic;

namespace Products.Services
{
    /// <summary>
    /// A mock service in a demo service layer. This layer, the client facing layer of the repository/service business layer, simply delegates to the repository
    /// </summary>
    public class ProductService : IProductService
    {
        // the service is permanently the client of a particular repository
        private readonly IProductRepository ProductRepository;

        public ProductService(IProductRepository productRepository)
        {
            // using this to clarify which is the parameter and which is the field
            this.ProductRepository = productRepository;
        }

        /// <summary>
        /// using a property rather than an accessor function GetProducts()
        /// this service doesn't actually add or change anything to the products list it gets from the repository
        /// </summary>
        IList<ProductModel> IProductService.Products => this.ProductRepository.Products;
    }
}
﻿using Products.Models;

using System.Collections.Generic;

namespace Products.Services
{/// <summary>
/// Using an interface for loose coupling even in a demo
/// </summary>
    public interface IProductService
    {
        public IList<ProductModel> Products { get; }
    }
}
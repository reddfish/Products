﻿namespace Products.Models
{
    /// <summary>
    /// A model of a simple product for a demo that only lists products in a mock database
    /// </summary>
    public class ProductModel
    {
        // a string identifier because arithmetic doesn't apply, 001<>1
        public string ProductId { get; set; }

        public string Name { get; set; }
        public decimal Price { get; set; }
        public int Stock { get; set; }

        // using data URIs for these demo images
        public string Image { get; set; }
    }
}
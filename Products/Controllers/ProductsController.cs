﻿using Microsoft.AspNetCore.Mvc;

using Products.Repositories;
using Products.Services;

namespace Products.Controllers
{
    /// <summary>
    /// Minimal controller that simply displays the list of products when the page is viewed
    /// </summary>
    public class ProductsController : Controller
    {
        // writing to interface unless there's a good reason why not
        private IProductService productService = new ProductService(new ProductRepository());

        public IActionResult Index()
        {
            return View(productService.Products);
        }
    }
}